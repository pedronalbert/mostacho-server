/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

	'get /champions/:id/battles': 'ChampionsController.battles',
	'get /battles/:id' : 'BattlesController.show',
	'post /battles/vote': 'BattlesController.vote',

  /*-------------RIOT API-----------------------*/
  


  'get /riot/match/:id': 'RiotController.matchFind',


  /*---------------------------Refact Api-------------------*/
  //Summoners
  'get /riot/summoner/:id/games/recent': 'RiotController.gamesRecent',
  'get /riot/summoner/:id/stats/ranked': 'RiotController.statsRanked',
  'get /riot/summoner/:id/stats/summary': 'RiotController.statsSummary',
  'get /riot/summoner/:id/match/history': 'RiotController.matchHistory',
  'get /riot/summoners/:ids/runes': 'RiotController.summonersRunes',
  'get /riot/summoners/:ids/masteries': 'RiotController.summonersMasteries',
  'get /riot/summoners/:ids/league': 'RiotController.league',
  'get /riot/summoners/:ids/league/entry': 'RiotController.leagueEntry',
  'get /riot/summoner/:id/game/current': 'RiotController.currentGame',

  //Posts
  'get /riot/summoner/by-id/:id': 'RiotController.summonerById',
  'post /riot/summoner/by-name': 'RiotController.summonerByName',


  //Static Data
  'get /riot/static-data/champions': 'RiotController.staticChampions',
  'get /riot/static-data/items': 'RiotController.staticItems',
  'get /riot/static-data/masteries': 'RiotController.staticMasteries',
  'get /riot/static-data/runes': 'RiotController.staticRunes'
  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  *  If a request to a URL doesn't match any of the custom routes above, it  *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
