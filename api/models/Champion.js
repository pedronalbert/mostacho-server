module.exports = {
	tableName: 'champions',

	attributes: {
		name: 'string',
		subName: 'string'
	}
};
