module.exports = {
	tableName: 'votes',

	attributes: {
		champion: 'integer',
		opponent: 'integer',
    type: 'string',
		battle: {
			model: 'battle'
		}
	}
}
