var Promise = require('bluebird');

module.exports = {
	tableName: 'battles',

	attributes: {
		champion: {
			model: 'champion',
			required: true
		},
		opponent: {
			model: 'champion',
			required: true
		},
		votes: {
			collection: 'vote',
			via: 'battle'
		}
	},

	beforeCreate: function (inputData, next) {
		RiotService.champions('lan')
			.then(function (champions) {
				if(_.isUndefined(_.find(champions, {id: inputData.champion}))) return next('Error');
				if(_.isUndefined(_.find(champions, {id: inputData.opponent}))) return next('Error');

				return next();
			})
			.catch(function (err) {
				console.log('error en el RiotService');
				next('error');
			});
	},

	afterCreate: function (battle, next) {
		var types = ['farming', 'killing', 'early', 'late'];

		_.forEach(types, function (type) {
			Vote.create({
				battle: battle.id,
				type: type,
				champion: 0,
				opponent: 0
			}).then(function (vote) {
				sails.log('Battle created');
			})
		})

		next();
	},

	findBattle: function (champions) {
		return new Promise(function (resolve, reject) {
			Battle.findOne()
				.where({or: [
					{champion: champions[0], opponent: champions[1]},
					{champion: champions[1], opponent: champions[0]}
					]})
				.populate('votes')
					.then(function(battle) {
						resolve(battle);
					})
					.catch(function (error) {
						reject('connectServerError');
					})
		})

	},

	findWithResults: function (championId) {
		return new Promise(function (resolve, reject) {
			Battle.find()
				.where({or: [
					{champion: championId},
					{opponent: championId}
					]})
					.populate('votes')
					.then(orderData)
					.catch(function (error) {
						reject(error);
					})

			function orderData(battles) {
				var championPercent;
				var nBattles;
				var aux;

				_.forEach(battles, function (battle) {
					championPercent = 0;
					nBattles = 0;
					if(parseInt(battle.champion) !== parseInt(championId) ) {
						aux = _.clone(battle.champion);
						battle.champion = battle.opponent;
						battle.opponent = aux;

						_.forEach(battle.votes, function (vote) {
							aux = vote.champion;
							vote.champion = vote.opponent;
							vote.opponent = aux;
							championPercent += ((vote.champion*100) / (vote.champion + vote.opponent));
							nBattles++;
						})


					} else {
						_.forEach(battle.votes, function (vote) {
							championPercent += ((vote.champion*100) / (vote.champion + vote.opponent));
							nBattles++;
						})
					}

					battle.championPercent = championPercent / nBattles;
					battle.opponentPercent = 100 - battle.championPercent;
				})
		
				resolve(battles);
			}
		})
	}
};
