var Promise = require('bluebird');

module.exports = {
  processVotes: function (inputData) {
    sails.log('processVotes called');

    return new Promise(function (resolve, reject) {
      validateVotes();

      function validateVotes() {
        _.forEach(inputData.battles, function (inputBattle) {
          if(parseInt(inputBattle.votes[0]) + parseInt(inputBattle.votes[1]) > 1) {
            reject('connectServerError');
          }

          if(parseInt(inputBattle.votes[0]) + parseInt(inputBattle.votes[1]) < 1) {
            reject('fillEmptyFields');
          }

        })

        findBattle();
      }

      function findBattle() {
        sails.log('findBattle called');

        Battle.findBattle(inputData.champions)
          .then(function (battle) {
            if(!_.isEmpty(battle)) {
              orderInputData(battle);
              assignVotes(battle);
            }else {
              createNewBattle();
            }
          })
          .catch(function (err) {
            reject('connectServerError');
          })
      }

      function orderInputData(battle) {
        sails.log('Order input data called');
        if(_.isEqual(parseInt(inputData.champions[0]), parseInt(battle.champion))) {
          sails.log('Data doesnt need order it');
        }else {
          inputData.champions.reverse();

          _.forEach(inputData.battles, function (battle) {
            battle.votes.reverse();
          })

          sails.log('inputData ordered');
        }
      }

      function assignVotes(battle) {
        var vote;

        sails.log('assignVotes called');

        _.forEach(inputData.battles, function (inputBattle) {
          vote = _.find(battle.votes, {type: inputBattle.type});

          if(!_.isEmpty(vote)) {
            inputVoteForChampion = parseInt(inputBattle.votes[0]);
            inputVoteForOpponent = parseInt(inputBattle.votes[1]);

            if(inputVoteForChampion === 1) vote.champion++;
            if(inputVoteForOpponent === 1) vote.opponent++;

              Vote.update({id: vote.id},{champion: vote.champion, opponent: vote.opponent})
                .then(function () {

                })
                .catch(function () {
                  reject('connectServerError');
                })
            } else {
            reject('connectServerError');
            }
        })
        resolve();
      }


      function createNewBattle() {
        sails.log('createNewBattle called');

        Battle.create({champion: inputData.champions[0], opponent: inputData.champions[1]})
          .then(function (battle) {
            findBattle();
          })
          .catch(function (err){
            reject('pleaseSelectTwoChampions');
          });
      }

    })
  }
}
