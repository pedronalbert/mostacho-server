var Promise = require('bluebird');
var Request = require('superagent');
var Riot = {
  baseUrl: 'https://na.api.pvp.net',
  key: '9c3b2810-5a71-4b8b-8d1f-75b6b96908c1',
};

module.exports = {
  champions: function(region, champData) {
    var url = Riot.baseUrl + '/api/lol/static-data/' + region + '/v1.2/champion';

    return new Promise(function (resolve, reject) {
      Request
        .get(url)
        .query({champData: champData})
        .query({api_key: Riot.key})
        .end(function (err, res) {
          if(err) return reject('riotServiceError');
          sails.log('riot status: ' + res.status);
          if(res.ok) {
            return resolve(_.sortBy(res.body.data, 'name'));
          } else if (res.status === 503) {
            return reject('riotServiceUnavailable');
          } else {
            return reject('connectServerError')
          }
        })
    });
  },

  items: function(region, itemListData) {
    var url = Riot.baseUrl + '/api/lol/static-data/' + region + '/v1.2/item';

    return new Promise(function (resolve, reject) {
      Request
        .get(url)
        .query({itemListData: itemListData})
        .query({api_key: Riot.key})
        .end(function (err, res) {
          if(err) return reject('riotServiceError');
          sails.log('riot status: ' + res.status);
          if(res.ok) {
            return resolve(_.toArray(res.body.data));
          } else if (res.status === 503) {
            return reject('riotServiceUnavailable');
          } else {
            return reject('connectServerError')
          }
        })
    });
  },

  masteries: function(region, masteryListData) {
    var url = Riot.baseUrl + '/api/lol/static-data/' + region + '/v1.2/mastery';

    return new Promise(function (resolve, reject) {
      Request
        .get(url)
        .query({masteryListData: masteryListData})
        .query({api_key: Riot.key})
        .end(function (err, res) {
          if(err) return reject('riotServiceError');
          sails.log('riot status: ' + res.status);
          if(res.ok) {
            return resolve(_.toArray(res.body.data));
          } else if (res.status === 503) {
            return reject('riotServiceUnavailable');
          } else {
            return reject('connectServerError')
          }
        })
    });
  },

  runes: function(region, runeListData) {
    var url = Riot.baseUrl + '/api/lol/static-data/' + region + '/v1.2/rune';

    return new Promise(function (resolve, reject) {
      Request
        .get(url)
        .query({runeListData: runeListData})
        .query({api_key: Riot.key})
        .end(function (err, res) {
          if(err) return reject('riotServiceError');
          sails.log('riot status: ' + res.status);
          if(res.ok) {
            return resolve(_.toArray(res.body.data));
          } else if (res.status === 503) {
            return reject('riotServiceUnavailable');
          } else {
            return reject('connectServerError')
          }
        })
    });
  },

  summonerByName: function (region, name) {
    name = name.toLowerCase();
    var formatName = name.replace(/\s+/g, '');
    var url = Riot.baseUrl + '/api/lol/' + region + '/v1.4/summoner/by-name/' + name;

    return new Promise(function (resolve, reject) {
      Request
        .get(url)
        .query({api_key: Riot.key})
        .end(function (err, res) {
          if(err) return reject('riotServiceError');
          sails.log('riot status: ' + res.status);
          if(res.ok) {
            return resolve(res.body[formatName]);
          } else if(res.notFound) {
            return reject('summonerNotFound');
          } else if (res.status === 503) {
            return reject('riotServiceUnavailable');
          } else {
            return reject('connectServerError')
          }
        })
    });
  },

  summonerById: function (region, id) {
    var url = Riot.baseUrl + '/api/lol/' + region + '/v1.4/summoner/' + id;

    return new Promise(function (resolve, reject) {
      Request
        .get(url)
        .query({api_key: Riot.key})
        .end(function (err, res) {
          if(err) return reject('riotServiceError');
          sails.log('riot status: ' + res.status);
          if(res.ok) {
            return resolve(res.body[id]);
          } else if(res.notFound) {
            return reject('summonerNotFound');
          } else if (res.status === 503) {
            return reject('riotServiceUnavailable');
          } else {
            return reject('connectServerError')
          }
        })
    });
  },

  summonersById: function (region, ids) {
    var url = Riot.baseUrl + '/api/lol/' + region + '/v1.4/summoner/' + ids;

    return new Promise(function (resolve, reject) {
      Request
        .get(url)
        .query({api_key: Riot.key})
        .end(function (err, res) {
          if(err) return reject('riotServiceError');
          sails.log('riot status: ' + res.status);
          if(res.ok) {
            return resolve(res.body);
          } else if(res.notFound) {
            return reject('summonersNotFound');
          } else if (res.status === 503) {
            return reject('riotServiceUnavailable');
          } else {
            return reject('connectServerError')
          }
        })
    });
  },

  summonersMasteries: function (region, ids) {
    var url = Riot.baseUrl + '/api/lol/' + region + '/v1.4/summoner/' + ids + '/masteries';

    return new Promise(function (resolve, reject) {
      Request
        .get(url)
        .query({api_key: Riot.key})
        .end(function (err, res) {
          if(err) return reject('riotServiceError');
          sails.log('riot status: ' + res.status);
          if(res.ok) {
            return resolve(res.body);
          } else if(res.notFound) {
            return reject('summonersMasteriesNotFound');
          }else if (res.status === 503) {
            return reject('riotServiceUnavailable');
          } else {
            return reject('connectServerError')
          }
        })
    });
  },

  summonersRunes: function (region, ids) {
    var url = Riot.baseUrl + '/api/lol/' + region + '/v1.4/summoner/' + ids + '/runes/';

    return new Promise(function (resolve, reject) {
      Request
        .get(url)
        .query({api_key: Riot.key})
        .end(function (err, res) {
          if(err) return reject('riotServiceError');
          sails.log('riot status: ' + res.status);
          if(res.ok) {
            return resolve(res.body);
          } else if(res.notFound) {
            return reject('summonerRunesNotFound');
          } else if (res.status === 503) {
            return reject('riotServiceUnavailable');
          } else {
            return reject('connectServerError')
          }
        })
    });
  },

  matchHistory: function (region, id) {
    var url = Riot.baseUrl + '/api/lol/' + region + '/v2.2/matchhistory/' + id ;

    return new Promise(function (resolve, reject) {
      Request
        .get(url)
        .query({api_key: Riot.key})
        .end(function(res) {
          sails.log('riot status: ' + res.status);
          if(res.ok) {
            return resolve(res.body);
          } else if(res.notFound) {
            return reject('summonerMatchHistoryNotFound');
          } else if (res.status === 503) {
            return reject('riotServiceUnavailable');
          } else {
            return reject('connectServerError')
          }
        })
    });
  },

  matchFind: function (region, id) {
    var url = Riot.baseUrl + '/api/lol/' + region + '/v2.2/match/' + id ;

    return new Promise(function (resolve, reject) {
      Request
        .get(url)
        .query({api_key: Riot.key})
        .end(function (err, res) {
          if(err) return reject('riotServiceError');
          sails.log('riot status: ' + res.status);
          if(res.ok) {
            return resolve(res.body);
          } else if(res.notFound) {
            return reject('matchNotFound');
          } else if (res.status === 503) {
            return reject('riotServiceUnavailable');
          } else {
            return reject('connectServerError')
          }
        })
    });
  },

  gamesRecent: function (region, id) {
    var url = Riot.baseUrl + '/api/lol/' + region + '/v1.3/game/by-summoner/' + id  + '/recent';

    return new Promise(function (resolve, reject) {
      Request
        .get(url)
        .query({api_key: Riot.key})
        .end(function (err, res) {
          if(err) return reject('riotServiceError');
          sails.log('riot status: ' + res.status);
          if(res.ok) {
            return resolve(res.body['games']);
          } else if (res.notFound) {
            return reject('summonerGamesRecentNotFound');
          } else if (res.status === 503) {
            return reject('riotServiceUnavailable');
          } else {
            return reject('connectServerError')
          }
        })
    });
  },

  statsRanked: function (region, id, season) {
    var url = Riot.baseUrl + '/api/lol/' + region + '/v1.3/stats/by-summoner/' + id  + '/ranked';

    return new Promise(function (resolve, reject) {
      Request
        .get(url)
        .query({season: season})
        .query({api_key: Riot.key})
        .end(function (err, res) {
          if(err) return reject('riotServiceError');
          sails.log('riot status: ' + res.status);
          if(res.ok) {
            return resolve(res.body);
          } else if(res.notFound) {
            return reject('summonerStatsRankedNotFound');
          } else if (res.status === 503) {
            return reject('riotServiceUnavailable');
          } else {
            return reject('connectServerError')
          }
        })
    });
  },

  statsSummary: function (region, id, season) {
    var url = Riot.baseUrl + '/api/lol/' + region + '/v1.3/stats/by-summoner/' + id  + '/summary';

    return new Promise(function (resolve, reject) {
      Request
        .get(url)
        .query({season: season})
        .query({api_key: Riot.key})
        .end(function (err, res) {
          if(err) return reject('riotServiceError');
          sails.log('riot status: ' + res.status);
          if(res.ok) {
            return resolve(res.body);
          } else if(res.notFound) {
            return reject('summonerStatsSummaryNotFound');
          } else if (res.status === 503) {
            return reject('riotServiceUnavailable');
          } else {
            return reject('connectServerError')
          }
        })
    });
  },

  league: function (region, ids) {
    var url = Riot.baseUrl + '/api/lol/' + region + '/v2.5/league/by-summoner/' + ids;

    return new Promise(function (resolve, reject) {
      Request
        .get(url)
        .query({api_key: Riot.key})
        .end(function (err, res) {
          if(err) return reject('riotServiceError');
          sails.log('riot status: ' + res.status);
          if(res.ok) {
            resolve(res.body);
          } else if(res.notFound) {
            reject('summonerLeagueNotFound');
          } else if (res.status === 503) {
            return reject('riotServiceUnavailable');
          } else {
            return reject('connectServerError')
          }
        });
    });
  },

  leagueEntry: function (region, ids) {
    var url = Riot.baseUrl + '/api/lol/' + region + '/v2.5/league/by-summoner/' + ids + '/entry';

    return new Promise(function (resolve, reject) {
      sails.log('init request');
      Request
        .get(url)
        .query({api_key: Riot.key})
        .end(function (err, res) {
          if(err) return reject('riotServiceError');
          sails.log('riot status: ' + res.status);
          if(res.ok || res.notFound) {
            setUnrankeds(res.body)
              .then(function (entries) {
                resolve(entries)
              })
              .catch(function (err) {
                reject(err);
              })
          } else if (res.status === 503) {
            return reject('riotServiceUnavailable');
          } else {
            return reject('connectServerError')
          }

          function setUnrankeds(entries) {
            return new Promise(function (resolve, reject) {

              RiotService.summonersById(region, ids)
                .then(function (summoners) {
                  _.forEach(summoners, function (summoner) {
                    var summonerLeagueEntry = entries[summoner.id];
                    sails.log(summonerLeagueEntry);
                    if(_.isUndefined(summonerLeagueEntry)) {
                      setNewLeagueEntryUnranked(summoner, entries);
                    } else if(_.isUndefined(_.find(summonerLeagueEntry, {queue: 'RANKED_SOLO_5x5'}))) {
                      setLeagueEntryUnranked(summoner, summonerLeagueEntry);
                    }

                    resolve(entries);
                  });
                })
                .catch(function (err) {
                  sails.log(err);
                  return reject(err);
                })
            })
          }

          function setNewLeagueEntryUnranked(summoner, entries) {
            sails.log('setNewLeagueEntryUnranked');
            var newUnranked = {};
            newUnranked['queue'] = 'RANKED_SOLO_5x5';
            newUnranked['tier'] = 'UNRANKED';
            newUnranked['entries'] = [{}];
            newUnranked['entries'][0]['leaguePoints'] = 0;
            newUnranked['entries'][0]['losses'] = 0;
            newUnranked['entries'][0]['playerOrTeamName'] = summoner.name;
            newUnranked['entries'][0]['playerOrTeamId'] = summoner.id;
            newUnranked['entries'][0]['wins'] = 0;
            newUnranked['entries'][0]['division'] = "0";

            entries[summoner.id] = [newUnranked];
          }

          function setLeagueEntryUnranked(summoner, entry) {
            sails.log('setLeagueEntryUnranked()');

            var unranked = {};
            unranked['queue'] = 'RANKED_SOLO_5x5';
            unranked['tier'] = 'UNRANKED';
            unranked['division'] = "0";
            unranked['entries'] = [{}];
            unranked['entries'][0]['leaguePoints'] = 0;
            unranked['entries'][0]['losses'] = 0;
            unranked['entries'][0]['playerOrTeamName'] = summoner.name;
            unranked['entries'][0]['playerOrTeamId'] = summoner.id;
            unranked['entries'][0]['wins'] = 0;
            unranked['entries'][0]['division'] = "0";

            entry.unshift(unranked);
          }

        });
    });
  },

  currentGame: function (region, id) {
    var platform;

    return new Promise(function (resolve, reject) {
      sails.log('searchGame called');
      if(region === 'lan') platform = 'LA1';
      if(region === 'las') platform = 'LA2';
      if(region === 'na') platform = 'NA1';
      if(region === 'br') platform = 'BR1';
      if(region === 'oce') platform = 'OC1';
      if(region === 'eune') platform = 'EUN1';
      if(region === 'tr') platform = 'TR1';
      if(region === 'ru') platform = 'RU';
      if(region === 'euw') platform = 'EUW1';
      if(region === 'kr') platform = 'KR';

      Request
        .get( Riot.baseUrl + '/observer-mode/rest/consumer/getSpectatorGameInfo/' + platform + '/' + id)
        .query({api_key: Riot.key})
        .end(function (err, res) {
          if(err) return reject('riotServiceError');
          sails.log('riot status: ' + res.status);
          if(res.ok) {
            var summonersIds = [];
            var game = res.body;

            //Save the summonersIds
            _.forEach(game.participants, function (participant) {
              summonersIds.push(participant.summonerId);
            })

            setLeagueEntryInfo(game, summonersIds)
              .then(function () {
                sails.log('resolved');
                resolve(game);
              })
              .catch(reject)

            function setLeagueEntryInfo(game, summonersIds) {
              sails.log('summonerLeagueEntryInfo()');
              return new Promise(function (resolve, reject) {
                RiotService.leagueEntry(region, summonersIds.toString())
                  .then(function (entries) {
                    //The key has the summoner id
                    _.forEach(entries, function (entry, key) {
                      var gameSummoner =_.find(game.participants, {summonerId: parseInt(key)});
                      gameSummoner.leagueEntry = entry;
                    })
                    sails.log('League entries setted');
                    resolve()
                  }) 
                  .catch(reject);
              })
            }

          } else if(res.notFound) {
            reject('summonerIsNotInGame');
          } else if (res.status === 503) {
            return reject('riotServiceUnavailable');
          } else {
            return reject('connectServerError')
          }
        })
    });
  }
}