module.exports = {
  calculatePercents: function (battle) {
    sails.log('calculate percents called');
    var championTotalPercent = 0;
    var opponenTotalPercent = 0;

    _.forEach(battle.votes, function (vote) {
      vote.championPercent = (vote.champion * 100) / (vote.champion + vote.opponent);
      vote.opponentPercent = (vote.opponent * 100) / (vote.champion + vote.opponent);
      championTotalPercent += vote.championPercent;
      opponenTotalPercent += vote.opponentPercent;
      vote.championPercent = parseFloat(vote.championPercent).toFixed(2);
      vote.opponentPercent = parseFloat(vote.opponentPercent).toFixed(2);
    });

    battle.championPercent = parseFloat(championTotalPercent / battle.votes.length).toFixed(2);
    battle.opponentPercent = parseFloat(opponenTotalPercent / battle.votes.length).toFixed(2);


    return battle;
  },
}