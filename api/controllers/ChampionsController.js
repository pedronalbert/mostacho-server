module.exports = {

  battles: function (req, res) {
    var user_language = _.isUndefined(req.param('user_language')) ? 'es' : req.param('user_language');
    
    Battle.findWithResults(req.params.id)
      .then(function (battles) {
        battles = { battles: battles };
        return res.success(battles);
      })
      .catch(function (err) {
        return res.error(err);
      });
  }
};
