module.exports = {
  staticChampions: function (req, res) {
    var region = req.param('region');
    var champData = req.param('champData');

    RiotService.champions(region, champData)
      .then(function (champions) {
        champions = {champions: champions};

        return res.success(champions);
      })
      .catch(function (err) {
        return res.error(err);
      })
  },

  staticItems: function (req, res) {
    var region = req.param('region');
    var itemListData = req.param('itemListData');

    RiotService.items(region, itemListData)
      .then(function (items) {
        items = {items: items};

        return res.success(items);
      })
      .catch(function (err) {
        return res.error(err);
      })
  },

  staticMasteries: function (req, res) {
    var region = req.param('region');
    var masteryListData = req.param('masteryListData');

    RiotService.masteries(region, masteryListData)
      .then(function (masteries) {
        masteries = {masteries: masteries};

        return res.success(masteries);
      })
      .catch(function (err) {
        return res.error(err);
      })
  },

  staticRunes: function (req, res) {
    var region = req.param('region');
    var runeListData = req.param('runeListData');

    RiotService.runes(region, runeListData)
      .then(function (runes) {
        runes = {runes: runes};

        return res.success(runes);
      })
      .catch(function (err) {
        return res.error(err);
      })
  },

  summonerByName: function (req, res) {
    var region = req.param('region');
    var name = req.param('name');

    RiotService.summonerByName(region, name)
      .then(function (summoner) {

        return res.success(summoner);
      })
      .catch(function (err) {
        return res.error(err);
      })
  },

  summonerById: function (req, res) {
    var region = req.param('region');
    var id = req.param('id');

    RiotService.summonerById(region, id)
      .then(function (summoner) {

        return res.success(summoner);
      })
      .catch(function (err) {
        return res.error(err);
      })
  },

  summonersMasteries: function (req, res) {
    var region = req.param('region');
    var ids = req.param('ids');

    RiotService.summonersMasteries(region, ids)
      .then(function (masteries) {

        return res.success(masteries);
      })
      .catch(function (err) {
        return res.error(err);
      })
  },

  summonersRunes: function (req, res) {
    var region = req.param('region');
    var ids = req.param('ids');

    RiotService.summonersRunes(region, ids)
      .then(function (runes) {

        return res.success(runes);
      })
      .catch(function (err) {
        return res.error(err);
      })
  },

  matchHistory: function (req, res) {
    var region = req.param('region');
    var id = req.param('id');

    RiotService.matchHistory(region, id)
      .then(function (matchHistory) {
        matchHistory = {matchHistory: matchHistory};

        return res.success(matchHistory);
      })
      .catch(function (err) {
        return res.error(err);
      })
  },

  matchFind: function (req, res) {
    var region = req.param('region');
    var id = req.param('id');

    RiotService.matchFind(region, id)
      .then(function (match) {
        match = {match: match};

        return res.success(match);
      })
      .catch(function (err) {
        return res.error(err);
      })
  },

  gamesRecent: function (req, res) {
    var region = req.param('region');
    var id = req.param('id');

    RiotService.gamesRecent(region, id)
      .then(function (gamesRecent) {

        return res.success(gamesRecent);
      })
      .catch(function (err) {
        return res.error(err);
      })
  },

  statsRanked: function (req, res) {
    var region = req.param('region');
    var id = req.param('id');
    var season = req.param('season');

    RiotService.statsRanked(region, id, season)
      .then(function (statsRanked) {

        return res.success(statsRanked);
      })
      .catch(function (err) {
        return res.error(err);
      })
  },

  statsSummary: function (req, res) {
    var region = req.param('region');
    var id = req.param('id');
    var season = req.param('season');

    RiotService.statsSummary(region, id, season)
      .then(function (statsSummary) {

        return res.success(statsSummary);
      })
      .catch(function (err) {
        return res.error(err);
      })
  },

  league: function (req, res) {
    var region = req.param('region');
    var ids = req.param('ids');

    RiotService.league(region, ids)
      .then(function (league) {

        return res.success(league);
      })
      .catch(function (err) {
        return res.error(err);
      })
  },

  leagueEntry: function (req, res) {
    var region = req.param('region');
    var ids = req.param('ids');

    RiotService.leagueEntry(region, ids)
      .then(function (leagueEntry) {

        return res.success(leagueEntry);
      })
      .catch(function (err) {
        return res.error(err);
      })
  },

  currentGame: function (req, res) {
    var id = req.param('id');
    var region = req.param('region');

    RiotService.currentGame(region, id)
      .then(function (game) {
        return res.success(game);
      })
      .catch(function (err) {
        return res.error(err);
      });
  }

}