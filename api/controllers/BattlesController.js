module.exports = {
  show: function (req, res) {
    Battle.findOne({id: req.params.id})
      .populate('votes')
      .then(function (battle) {
        battle = BattleService.calculatePercents(battle);
        battle = {battle: battle};
        return res.success(battle);
      })
      .catch(function (err) {
        return res.error(err);
      });
  },

  vote: function (req, res) {
    VoteService.processVotes(req.allParams())
      .then(function (vote) {
        return res.success('voteAdded');
      })
      .catch(function (err) {
        return res.error(err);
      });
  }

};
