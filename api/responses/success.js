module.exports = function success (data) {
  var req = this.req;
  var res = this.res;
  var format = {};
  var user_language = _.isUndefined(req.param('user_language')) ? 'es' : req.param('user_language');

  format.status = 'success';

  if(_.isArray(data) || _.isObject(data)) {
    format.data = data;
  } else if (_.isString(data)) {
    format.message = req.__(data);
  }


  res.json(format);
}
