module.exports = function error(err) {
  var req = this.req;
  var res = this.res;
  var user_language = _.isUndefined(req.param('user_language')) ? 'es' : req.param('user_language');
  req.setLocale(user_language);
  sails.log('language: ' + user_language);
  
  res.json( {
    status: 'error',
    message: req.__(err)
  });

};